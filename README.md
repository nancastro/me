# Hi, I'm Renan

This is a personal README for you to get to know me better.

💼 My portfolio - [renancastro.com](https://renancastro.com/) <br>
🔗 LinkedIn - [linkedin.com/in/nancastro](https://www.linkedin.com/in/nancastro)

## About me

- Born and raised in Brazil, I migrated to Canada in 2010, became a Canadian Citizen, moved to Australia with my wife in 2015 and also became Australian 🇧🇷🇨🇦🇦🇺 Next, I'm planing to live in Europe for a while 😁
- I'm a Product Designer who started as a Graphic/Web Designer back in 2005 and got my first role as a UX Designer in 2012. Detailed work history on [LinkedIn](https://www.linkedin.com/in/nancastro). I have also been mentoring others since 2021 on platforms such as [Springboard](https://springboard.com/) and [Harness Projects](https://www.harnessprojects.com.au/).
- I am a generalist who can perform above average in many fields and trusts specialists to do a much better job in their area of expertise.
- I believe there are many ways to solve a problem, and the best solutions come from a multidisciplinary and diverse team of collaborative people.
- My motto and main advice for teammates is to fall in love with the problem you're trying to solve and not get attached to the solution you're working on.
- Data excites me, and discussions/decisions based on it are a huge motivator. Opinion-based conversations have the opposite effect.
- I love learning and experiencing new things professionally and personally, mainly related to design, technology, front-end development, no-code, travel, financial independence, health, fitness, and nutrition.

## Lean UX evangelist

I like using some of the [Lean UX](https://www.interaction-design.org/literature/article/a-simple-introduction-to-lean-ux) principles to guide my work:

- **Progress = Outcomes, Not Outputs** - Features and services are outputs. The outcomes are the experiences we create and the consequences of those experiences.
- **Problem-Focused Teams** - Give a problem for your team to solve instead of a set of features to implement.
- **Get Out of the Building** - Don't make decisions solely based on your office discussions. Go outside and get feedback from those who use your product.
- **Learning over Growth** - Learn first and scale second. Scaling an unproven idea is too risky.
- **Permission to Fail** - To achieve the best solution, one must experiment. Experimentation breeds creativity, and creativity yields innovative solutions. If the team fears for their jobs, they won't take risks to create something extraordinary.

## Joining GitLab

As I research more about GitLab, I get excited about the possibility of working in a place focused on the right [CREDIT](https://handbook.gitlab.com/handbook/values/) and where design is truly integrated into the process. It would be a great pleasure to work in a strong [UX Department](https://about.gitlab.com/handbook/product/ux/) and to contribute to [Pajamas Design System](https://design.gitlab.com), as well as to learn from teammates and to share my knowledge with others.

I believe I would be a great asset to GitLab as I bring years of experience working with various development teams from different corners of the globe. From scoping a project to monitoring it after implementation and all phases in between (e.g. user research, prototyping, usability testing), I also have experience with data analysis and front-end development. My happy place leans towards the intersection of design and development.

It would be great if I could join GitLab some day 🤞

## Footnotes

This document was inspired by the [UX Department's About our Team section](https://about.gitlab.com/handbook/engineering/ux/index.html#about-our-team) and the trend of Engineering Manager READMEs.
